﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VimutMpayWs.Core;
using VimutMpayWs.Models;
using VimutMpayWs.Models.PaymentModel;

namespace VimutMpayWs.Controllers
{
    [RoutePrefix("api/payment")]
    public class PaymentController : ApiController
    {
        private readonly ISQLManger _sql;
        private readonly IMpayService _mpay;

        public PaymentController(ISQLManger sql, IMpayService mpay)
        {
            _sql = sql;
            _mpay = mpay;
        }

        [HttpPost]
        [Route("generateqr")]
        public CommonResponse GenerateQR([FromBody] GenerateQRRequest request)
        {
            CommonResponse response = new CommonResponse();
            int seqId = 0;
            string messageEx = string.Empty;
            try
            {
                //seqId = _sql.InsertLogInternalService(0, "ReceivePaidResponse", "");
                var result = _mpay.GenerateQR(request);
                if (result.successResponse != null)
                {
                    response = new CommonResponse
                    {
                        code = 200,
                        status = true,
                        message = JsonConvert.SerializeObject(result.successResponse)
                    };
                }
                else
                {
                    response = new CommonResponse
                    {
                        code = 200,
                        status = false,
                        message = JsonConvert.SerializeObject(result.errorResponse)
                    };
                }

            }
            catch (Exception ex)
            {
                messageEx = ex.ToString();
                response = new CommonResponse
                {
                    code = 999,
                    status = false,
                    message = ex.ToString()
                };
            }
            finally
            {
                if (seqId > 0)
                {
                    //_sql.UpdateLogInternalService(seqId, JsonConvert.SerializeObject(response), messageEx);
                }
            }
            return response;
        }
              
        [HttpPost]
        [Route("order")]
        public CommonResponse GetPaymentOrder([FromBody]PaymentOrderRequest request)
        {
            CommonResponse response = new CommonResponse();
            int seqId = 0;
            string messageEx = string.Empty;
            try
            {
                //seqId = _sql.InsertLogInternalService(0, "GetPaymentOrder", "");
                var result = _mpay.PaymentOrder(request);
                if (result.successResponse != null)
                {
                    response = new CommonResponse
                    {
                        code = 200,
                        status = true,
                        message = JsonConvert.SerializeObject(result.successResponse)
                    };
                }
                else
                {
                    response = new CommonResponse
                    {
                        code = 200,
                        status = false,
                        message = JsonConvert.SerializeObject(result.errorResponse)
                    };
                }
            }
            catch (Exception ex)
            {
                messageEx = ex.ToString();
                response = new CommonResponse
                {
                    code = 999,
                    status = false,
                    message = ex.ToString()
                };
            }
            finally
            {
                if (seqId > 0)
                {
                    _sql.UpdateLogInternalService(seqId, JsonConvert.SerializeObject(response), messageEx);
                }
            }
            return response;
        }

        [HttpPost]
        [Route("receivepaidresponse")]
        public CommonResponse ReceivePaidResponse([FromBody]ReceivePaidResponseRequest request)
        {
            CommonResponse response = new CommonResponse();
            int seqId = 0;
            string messageEx = string.Empty;
            try
            {
                //seqId = _sql.InsertLogInternalService(0, "GetPaymentOrder", "");              
                response = new CommonResponse
                {
                    code = 200,
                    status = true,
                    message = JsonConvert.SerializeObject(request)
                };

            }
            catch (Exception ex)
            {
                messageEx = ex.ToString();
                response = new CommonResponse
                {
                    code = 999,
                    status = false,
                    message = ex.ToString()
                };
            }
            finally
            {
                if (seqId > 0)
                {
                    _sql.UpdateLogInternalService(seqId, JsonConvert.SerializeObject(response), messageEx);
                }
            }
            return response;
        }

    }
}
