﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models.PaymentModel
{
    public class PaymentOrderResponse
    {
        public string order_id { get; set; }
        public string txn_id { get; set; }
        public string txn_token { get; set; }
        public string status { get; set; }
        public string status_code { get; set; }
        public string status_message { get; set; }
        public string created_at { get; set; }
        public string currency { get; set; }
        public string cust_id { get; set; }
        public string amount { get; set; }
        public string amount_net { get; set; }
        public string amount_cust_fee { get; set; }
        public string form_url { get; set; }

        [JsonProperty("3ds")]
        public TDS tds;
    }
}

