﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models.PaymentModel
{
    public class TDS
    {
        [JsonProperty("3ds_required")]
        public bool required { get; set; }

        [JsonProperty("3ds_url_success")]
        public string success_url { get; set; }

        [JsonProperty("3ds_url_fail")]
        public string failed_url { get; set; }
    }
}