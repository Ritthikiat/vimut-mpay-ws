﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models.PaymentModel
{
    public class GenerateQRRequest
    {
        public string order_id { get; set; }
        public string product_name { get; set; }
        public string sof { get; set; }
        public string service_id { get; set; }
        public string location_name { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string expire_time_seconds { get; set; }
        public string ref_1 { get; set; }
        public string ref_2 { get; set; }
        public string ref_3 { get; set; }
        public string ref_4 { get; set; }
        public string ref_5 { get; set; }
    }
}