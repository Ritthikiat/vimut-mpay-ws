﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace VimutMpayWs.Models.PaymentModel
{
    public class ReceivePaidResponseRequest
    {
        public string order_id { get; set; }
        public string merchant_id { get; set; }
        public string txn_id { get; set; }
        public string status { get; set; }
        public string status_code { get; set; }
        public string status_message { get; set; }
        public string currency { get; set; }
        public decimal amount { get; set; }
        public decimal amount_net { get; set; }
        public decimal amount_cust_fee { get; set; }
        public string service_id { get; set; }
        public string channel { get; set; }

        public Card card;

        public Installment installment;

        public Bank bank;

        public Rlp rlp;

        public string sof_txn_id { get; set; }
        public string created_at { get; set; }
        public string success_at { get; set; }
    }
    public class Card
    {
        public string card_holder_name { get; set; }
        public string card_no { get; set; }
        public string card_type { get; set; }
        public string card_expire { get; set; }
        public string card_country { get; set; }
        public string card_ref { get; set; }
    }
    public class Installment
    {
        public string bank_issuer { get; set; }
        public int term { get; set; }
        public decimal amount_per_term { get; set; }
    }
    public class Bank
    {
        public string account_last_digits { get; set; }
        public string account_name { get; set; }
        public string bank_code { get; set; }
    }
    public class Rlp
    {
        public string pay_type { get; set; }
        public string token_Id { get; set; }

        public List<PayInfo> pay_info;

        public List<Package> packages;

    }
    public class PayInfo
    {
        public string method { get; set; }
        public decimal amount { get; set; }
        public string credit_card_nickname { get; set; }
        public string credit_card_brand { get; set; }

    }
    public class Package
    {
        public string id { get; set; }
        public decimal amount { get; set; }

        public List<Products> products;

    }
    public class Products
    {
        public string id { get; set; }
        public string name { get; set; }
        public string image_url { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }

    }

}