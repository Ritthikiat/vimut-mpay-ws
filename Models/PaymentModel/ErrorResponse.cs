﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models.PaymentModel
{

    public class ErrorResponse
    {
        public string errorCode { get; set; }
        public string error { get; set; }
        public string cause { get; set; }
        public string timestamp { get; set; }
        public string contextPath { get; set; }
        public string service { get; set; }
        public string traceId { get; set; }
    }
}