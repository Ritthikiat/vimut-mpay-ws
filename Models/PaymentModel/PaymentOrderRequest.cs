﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models.PaymentModel
{
    public class PaymentOrderRequest
    {
        public string order_id { get; set; }
        public string product_name { get; set; }
        public string service_id { get; set; }
        public string channel_type { get; set; }
        public string cust_id { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string ref_1 { get; set; }
        public string ref_2 { get; set; }
        //public string ref_3 { get; set; }
        //public string ref_4 { get; set; }
        //public string ref_5 { get; set; }
        public string form_type { get; set; }
        //public string skin_code { get; set; }
        public bool is_remember { get; set; }
        public object metadata { get; set; }

        [JsonProperty("3ds")]
        public TDS tds;
    }
  
}