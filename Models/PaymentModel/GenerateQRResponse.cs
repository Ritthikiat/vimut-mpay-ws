﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models.PaymentModel
{
    public class GenerateQRResponse
    {
        public string order_id { get; set; }
        public string txn_id { get; set; }
        public string qr_code { get; set; }
        public string qr_img { get; set; }
        public string amount { get; set; }
        public string amount_net { get; set; }
        public string amount_cust_fee { get; set; }
        public string currency { get; set; }
        public string created_at { get; set; }
        public string qr_expire_at { get; set; }
    }
}