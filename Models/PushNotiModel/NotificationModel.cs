﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models.PushNotiModel
{
    public class NotificationModel
    {
        public List<string> registration_ids { get; set; }

        public Notification notification;
        public string priority { get; set; }

        public Android android;

        public object data;
    }
    public class Notification
    {
        public string title { get; set; }
        public string body { get; set; }
    }
    public class Android
    {
        public string ttl { get; set; }

        public AndroidNoti notification;
    }
    public class AndroidNoti
    {
        public string click_action { get; set; }
        public string channel_id { get; set; }
    }
}