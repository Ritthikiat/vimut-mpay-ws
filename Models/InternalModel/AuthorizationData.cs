﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models.InternalModel
{
    public class AuthorizationData
    {
        public int accountId { get; set; }
        public string userAgent { get; set; }
        public string deviceId { get; set; }
        public string deviceToken { get; set; }
        public string versionIos { get; set; }
        public string versionAndroid { get; set; }
        public string alg { get; set; }
        public double createDate { get; set; }
        public double expireDate { get; set; }
    }
}