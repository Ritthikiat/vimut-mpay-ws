﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VimutMpayWs.Models
{
    public class CommonResponse
    {
        public int code { get; set; }
        public string message { get; set; }
        public bool status { get; set; }
    }
}