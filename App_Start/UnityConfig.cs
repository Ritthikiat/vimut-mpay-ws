using System.Web.Http;
using Unity;
using Unity.WebApi;
using VimutMpayWs.Core;

namespace VimutMpayWs
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            container.RegisterType<ISQLManger, SQLManager>();
            container.RegisterType<IAuthenticationService, Core.AuthenticationService>();
            container.RegisterType<IPushNotification, PushNotification>();
            container.RegisterType<IMpayService, MpayService>();
            // e.g. container.RegisterType<ITestService, TestService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}