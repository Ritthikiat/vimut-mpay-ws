﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using VimutMpayWs.Models;
using VimutMpayWs.Models.InternalModel;

namespace VimutMpayWs.Core
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly ISQLManger _sql;
        private static readonly string _apiKey = WebConfigurationManager.AppSettings["api_key"];
        public AuthenticationService(ISQLManger sql)
        {
            _sql = sql;
        }
        public AuthorizationData ValidateHeader(AuthenticationHeaderValue authorization)
        {
            AuthorizationData data = new AuthorizationData();
            try
            {
                var token = authorization.ToString();
                string[] data_auth = token.Split(' ');//แยก Barrer กับ ตัว Token
                if (data_auth.Length > 1)
                {
                    //string type = data_auth[0];  //Barrer
                    token = data_auth[1]; //Token String
                }
                data = AuthorizationDecode(token); // Decode Token 

                #region checktistokenauthen
                var isAuthen = _sql.CheckAuthorization(data.accountId,data.deviceId,token);
                if (!isAuthen)
                {
                    var commResp = new CommonResponse
                    {
                        code = 401,
                        status = false,
                        message = "Unauthorized"
                    };
                    var response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        StatusCode = HttpStatusCode.Unauthorized,
                        Content = new StringContent(JsonConvert.SerializeObject(commResp), System.Text.Encoding.UTF8, "application/json")
                    };
                    throw new HttpResponseException(response);
                }
                #endregion

                #region checkexpiretoken
                DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                dateTime = dateTime.AddSeconds(data.expireDate).ToLocalTime();
                bool status_expire = (DateTime.Now > dateTime) ? true : false; //check expire token
                if (status_expire)
                {
                    var commResp = new CommonResponse
                    {
                        code = 401,
                        status = false,
                        message = "Expired Token"
                    };
                    var response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                    {
                        StatusCode = HttpStatusCode.Unauthorized,
                        Content = new StringContent(JsonConvert.SerializeObject(commResp), System.Text.Encoding.UTF8, "application/json")
                    };
                    throw new HttpResponseException(response);
                }
                #endregion


            }
            catch (Exception)
            {
                var commResp = new CommonResponse
                {
                    code = 401,
                    status = false,
                    message = "Unauthorized"
                };
                var response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    StatusCode = HttpStatusCode.Unauthorized,
                    Content = new StringContent(JsonConvert.SerializeObject(commResp), System.Text.Encoding.UTF8, "application/json")
                };
                throw new HttpResponseException(response);
            }
            return data;
        }

        public string GenerateToken(string user_agent, string device_id,string device_token, string user_id, string signal)
        {
            try
            {
                double timestampNow = Utility.DateTimeToUnixTimestamp(DateTime.Now);
                double timestampYear = Utility.DateTimeToUnixTimestamp(DateTime.Now.AddMinutes(3600));

                string header = "";
                header = "{";
                header += " \"user_agent\":\"" + user_agent + "\",";
                header += " \"device_id\":\"" + device_id + "\",";
                header += " \"device_token\":\"" + device_token + "\",";
                header += " \"alg\":\"HS256\"";
                header += " }";

                string payload = "";
                payload = "{";
                payload += " \"expire_date\":" + timestampYear + ",";
                payload += " \"create_date\":" + timestampNow + ",";
                payload += " \"account_id\":" + user_id + "";
                payload += " }";

                string encryptedHeader = Utility.Base64UrlEncode(header);
                string encryptedPayload = Utility.Base64UrlEncode(payload);
                string encryptedSignature = Utility.sha256(encryptedHeader + "." + encryptedPayload + signal);
                string result = encryptedHeader + "." + encryptedPayload + "." + encryptedSignature;

                return result;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private static AuthorizationData AuthorizationDecode(string auth)
        {
            AuthorizationData dataAuth = new AuthorizationData();

            string[] data = auth.Split('.');
            if (data.Length != 3)
            {
                throw new Exception();
            }
            string header = data[0];
            string payload = data[1];
            string signature = data[2];

            try
            {
                string decodeHeader = Utility.Base64ForUrlDecode(header);
                string decodePayload = Utility.Base64ForUrlDecode(payload);

                JToken tokenHeader = JObject.Parse(decodeHeader);
                JToken tokenPayload = JObject.Parse(decodePayload);

                dataAuth.accountId = int.Parse(tokenPayload["account_id"].ToString());
                dataAuth.createDate = Convert.ToDouble(tokenPayload["create_date"].ToString());
                dataAuth.expireDate = Convert.ToDouble(tokenPayload["expire_date"].ToString());
                dataAuth.userAgent = tokenHeader["user_agent"].ToString();
                dataAuth.deviceId = tokenHeader["device_id"].ToString();
                dataAuth.deviceToken = tokenHeader["device_token"].ToString();
                dataAuth.alg = tokenHeader["alg"].ToString();
            }
            catch
            {
                throw new Exception();
            }

            return dataAuth;
        }

        public void ValidateAPIKey(string apikey)
        {            
            #region checkapikey
            //var data = apikey.Split('.');
            //var validateKey = data[0];
            //var expiredTime = data[1];
            if (apikey != _apiKey)
            {
                var commResp = new CommonResponse
                {
                    code = 400,
                    status = false,
                    message = "Bad API KEY"
                };
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject(commResp), System.Text.Encoding.UTF8, "application/json")
                };
                throw new HttpResponseException(response);
            }
            #endregion
        }
    }
}