﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using VimutMpayWs.Models.PushNotiModel;

namespace VimutMpayWs.Core
{
    public class PushNotification : IPushNotification
    {
        private readonly ISQLManger _sql;
        public PushNotification(ISQLManger sql)
        {
            _sql = sql;
        }
        public void PushPaymentNotification()
        {
            try
            {

            }
            catch(Exception ex)
            {

            }
        }          

        //public void SendMessage(int notiId, List<string> token, string eventText, string action ,string hn, object data, string title, string body)
        //{
        //    string serverKey = WebConfigurationManager.AppSettings["fcm_key"];

        //    try
        //    {
        //        var result = "-1";
        //        var webAddr = "https://fcm.googleapis.com/fcm/send";
        //        var logid = 0;
        //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
        //        httpWebRequest.ContentType = "application/json";
        //        httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
        //        // httpWebRequest.Headers.Add("Sender:key=" + senderId);
        //        httpWebRequest.Method = "POST";

        //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //        {
        //            // Construct payload based on each device platform

        //            NotificationModel model = new NotificationModel
        //            {
        //                registration_ids = token,
        //                notification = new Notification
        //                {
        //                    title = title,
        //                    body = body
        //                },
        //                android = new Android
        //                {
        //                    ttl = "86400s",
        //                    notification = new AndroidNoti
        //                    {
        //                        click_action = action,
        //                        channel_id = eventText
        //                    }
        //                },
        //                data = data
        //            };
        //            var bodyReq = JsonConvert.SerializeObject(model);
        //            logid = _sql.InsertLogPushNotification("PUSH NOTIFICATION", bodyReq, notiId);
        //            streamWriter.Write(bodyReq);
        //            streamWriter.Flush();
                
        //        }

        //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //        {
        //            result = streamReader.ReadToEnd();
        //        }
        //        _sql.UpdateLogPushNotification(logid, result);

        //        var response = JsonConvert.DeserializeObject<NotificationFireBaseModel>(result);
        //        if (response.success > 0)
        //        {
        //            _sql.UpdatePushNotiStatus(notiId);
        //        }
        //        else
        //        {
        //            _sql.UpdateRetryCount(notiId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //  Response.Write(ex.Message); 
        //        _sql.UpdateRetryCount(notiId);
        //    }
        //}
    }
}