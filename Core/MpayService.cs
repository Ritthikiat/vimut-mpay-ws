﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Web;
using System.Web.Http;
using VimutMpayWs.Models.PaymentModel;

namespace VimutMpayWs.Core
{
    public class MpayService : IMpayService
    {
        private static readonly HttpClient client = new HttpClient();
        private static string mpayDomain = "https://api.stg-paymentgateway.ais.co.th/stg";
        public MpayResponse PaymentOrder(PaymentOrderRequest request)
        {
            var url = mpayDomain + "/service-txn-gateway/v1/cc/txns/payment_order";
            var content = JsonConvert.SerializeObject(request);
            var requestMessage = new HttpRequestMessage()
            {
                Method = new HttpMethod("POST"),
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/json")
            };
            return ConnectRestApi<PaymentOrderResponse>(requestMessage);
        }

        //public void InquiryCardToken()
        //{
        //    var url = mpayDomain + "/service-txn-gateway/v1/cc/cards/inquiry";
        //    var content = JsonConvert.SerializeObject("");
        //    var requestMessage = new HttpRequestMessage()
        //    {
        //        Method = new HttpMethod("POST"),
        //        RequestUri = new Uri(url),
        //        Content = new StringContent(content, Encoding.UTF8, "application/json")
        //    };
        //    //return ConnectRestApi<ECommonResponse>(requestMessage);
        //}

        //public void TerminateCardToken()
        //{
        //    var url = mpayDomain + "/service-txn-gateway/v1/cc/cards/unregister";
        //    var content = JsonConvert.SerializeObject("");
        //    var requestMessage = new HttpRequestMessage()
        //    {
        //        Method = new HttpMethod("POST"),
        //        RequestUri = new Uri(url),
        //        Content = new StringContent(content, Encoding.UTF8, "application/json")
        //    };
        //    //return ConnectRestApi<ECommonResponse>(requestMessage);
        //}

        public MpayResponse GenerateQR(GenerateQRRequest request)
        {
            var url = mpayDomain + "/service-txn-gateway/v1/qr";
            var content = JsonConvert.SerializeObject(request);
            var requestMessage = new HttpRequestMessage()
            {
                Method = new HttpMethod("POST"),
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/json")
            };
            return ConnectRestApi<GenerateQRResponse>(requestMessage);
        }

        private MpayResponse ConnectRestApi<T>(HttpRequestMessage request) where T : class
        {
            MpayResponse mpayResp = new MpayResponse();
            try
            {
                var reqBody = request.Content.ReadAsStringAsync().Result;
                var guid = new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString();
                request.Headers.Add("X-sdpg-nonce", "1620884521");
                request.Headers.Add("X-sdpg-merchant-id", "2133901");
                request.Headers.Add("X-sdpg-signature", Utility.Signature("a700fa926193d0a5", reqBody + "1620884521"));

                var response = client.SendAsync(request);
                var responseBody = response.Result.Content.ReadAsStringAsync().Result;
                var jsonSettings = new JsonSerializerSettings
                {
                    MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                    DateParseHandling = DateParseHandling.None,
                    Converters =
                        {
                            new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
                        },
                    NullValueHandling = NullValueHandling.Include
                };
                if (response.Result.StatusCode == HttpStatusCode.OK) // OK = status code 200
                {
                    mpayResp.successResponse = JsonConvert.DeserializeObject<T>(responseBody.ToString(), jsonSettings);
                }
                else
                {
                    mpayResp.errorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseBody.ToString(), jsonSettings);
                }

                return mpayResp;                
              
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}