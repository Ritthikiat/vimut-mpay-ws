﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VimutMpayWs.Core
{
    public interface ISQLManger
    {
        int InsertLogInternalService(int userId, string serviceName, string request);
        void UpdateLogInternalService(int userId, string response, string messageEx);
        bool CheckAuthorization(int hospitalId, string deviceId, string token);
    }
}
