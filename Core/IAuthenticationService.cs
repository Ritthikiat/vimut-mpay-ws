﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using VimutMpayWs.Models.InternalModel;

namespace VimutMpayWs.Core
{
    public interface IAuthenticationService
    {
        AuthorizationData ValidateHeader(AuthenticationHeaderValue header);
        string GenerateToken(string user_agent,string device_id, string device_token, string user_id, string signal);
        void ValidateAPIKey(string apikey);
    }
}
