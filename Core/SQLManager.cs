﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;

namespace VimutMpayWs.Core
{
    public class SQLManager : ISQLManger
    {
        [ThreadStatic]
        private static SQLManager instance = null;

        public static SQLManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SQLManager();
                }
                return instance;
            }
        }

        public bool CheckAuthorization(int hospitalId, string deviceId, string token)
        {
            var success = false;
            DataSet ds = new DataSet();
            DataTable table = new DataTable();
            SQLCustomExecute sql = new SQLCustomExecute("check_authorization");

            NpgsqlParameter paramHosId = new NpgsqlParameter(@"phosid", NpgsqlDbType.Integer);
            paramHosId.Direction = ParameterDirection.Input;
            paramHosId.Value = hospitalId;

            NpgsqlParameter paramDeviceId = new NpgsqlParameter(@"pdeviceid", NpgsqlDbType.Varchar);
            paramDeviceId.Direction = ParameterDirection.Input;
            paramDeviceId.Value = deviceId;

            NpgsqlParameter paramToken = new NpgsqlParameter(@"ptoken", NpgsqlDbType.Varchar);
            paramToken.Direction = ParameterDirection.Input;
            paramToken.Value = token;

            sql.Parameters.Add(paramHosId);
            sql.Parameters.Add(paramDeviceId);
            sql.Parameters.Add(paramToken);

            ds = sql.executeQueryWithReturnDataset();

            if (ds != null && ds.Tables.Count > 0)
            {
                table = ds.Tables[0];
                if (table != null && table.Rows.Count > 0)
                {
                    success = true;
                }

            }
            return success;
        }

        public int InsertLogInternalService(int hospitalId, string serviceName, string request)
        {
            DataSet ds = new DataSet();
            DataTable table = new DataTable();
            SQLCustomExecute sql = new SQLCustomExecute("insert_log_internal_service");
            int id = 0;

            NpgsqlParameter paramHospitalId = new NpgsqlParameter(@"phosid", NpgsqlDbType.Integer);
            paramHospitalId.Direction = ParameterDirection.Input;
            paramHospitalId.Value = hospitalId;

            NpgsqlParameter paramServiceName = new NpgsqlParameter(@"pservicename", NpgsqlDbType.Varchar);
            paramServiceName.Direction = ParameterDirection.Input;
            paramServiceName.Value = serviceName;

            NpgsqlParameter paramReq = new NpgsqlParameter(@"prequest", NpgsqlDbType.Varchar);
            paramReq.Direction = ParameterDirection.Input;
            paramReq.Value = request;

            sql.Parameters.Add(paramHospitalId);
            sql.Parameters.Add(paramServiceName);
            sql.Parameters.Add(paramReq);

            ds = sql.executeQueryWithReturnDataset();

            if (ds != null && ds.Tables.Count > 0)
            {
                table = ds.Tables[0];
                if (table != null && table.Rows.Count > 0)
                {
                    DataRow dr = table.Rows[0];
                    id = Convert.ToInt32(dr["insertedId"].ToString());
                }

            }
            return id;
        }

        public void UpdateLogInternalService(int logId, string response, string messageEx)
        {
            DataSet ds = new DataSet();
            DataTable table = new DataTable();
            SQLCustomExecute sql = new SQLCustomExecute("update_log_internal_service");

            NpgsqlParameter paramId = new NpgsqlParameter(@"plogid", NpgsqlDbType.Integer);
            paramId.Direction = ParameterDirection.Input;
            paramId.Value = logId;

            NpgsqlParameter paramRes = new NpgsqlParameter(@"presponse", NpgsqlDbType.Varchar);
            paramRes.Direction = ParameterDirection.Input;
            paramRes.Value = response;

            NpgsqlParameter paramException = new NpgsqlParameter(@"pexception", NpgsqlDbType.Varchar);
            paramException.Direction = ParameterDirection.Input;
            paramException.Value = messageEx;

            sql.Parameters.Add(paramId);
            sql.Parameters.Add(paramRes);
            sql.Parameters.Add(paramException);

            ds = sql.executeQueryWithReturnDataset();
        }


        public class SQLCustomExecute
        {
            public List<NpgsqlParameter> Parameters { get; set; }
            public string sqlCommand { get; set; }


            public SQLCustomExecute(string sqlCommand)
            {
                this.sqlCommand = sqlCommand;
                this.Parameters = new List<NpgsqlParameter>();
            }

            public DataSet executeQueryWithReturnDataset()
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US", false);
                DataSet result = new DataSet();
                string connectionString = WebConfigurationManager.AppSettings["connectionStrings"];
                using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
                {
                    NpgsqlCommand command = new NpgsqlCommand();
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = this.sqlCommand;

                    if (this.Parameters != null)
                        foreach (NpgsqlParameter parameter in this.Parameters)
                            command.Parameters.Add(parameter);

                    try
                    {
                        connection.Open();
                        NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command);
                        adapter.Fill(result);
                    }
                    catch (Exception e)
                    {
                        result = null;
                    }
                    finally
                    {
                        connection.Dispose();
                        connection.Close();
                    }
                }

                return result;
            }
        }
    }
}