﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VimutMpayWs.Models.PaymentModel;

namespace VimutMpayWs.Core
{
    public interface IMpayService 
    {
        MpayResponse GenerateQR(GenerateQRRequest request);
        MpayResponse PaymentOrder(PaymentOrderRequest request);
    }
}
